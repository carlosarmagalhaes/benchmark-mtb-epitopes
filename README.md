**Project is being developed on a private fork. The full spectrum of code and data will be available after publication.**

# Benchmark MTB epitopes

Immunoinformatics
=====================

Benchmarking
-------
Experimentally validated T cell epitopes of the MTBC were extracted from the [IEDB database](https://www.iedb.org) on 30/11/2021 with the filters Linear peptide; Organism=Mycobacterium tuberculosis complex; Human host; class I or II HLA restriction. T cell and MHC ligand assays with positive and negative outcomes were exported separately. For each HLA of the DRB locus, we excluded peptides with fewer than nine amino acids, as well as those with both positive and negative epitope assignments.

A total of 1021 peptide epitopes (positive result in experimental assays) and 203 non-epitopes (negative result in experimental assays) were submitted to the CD4+ T cell epitope prediction tool NetMHCIIpan-4.0. The minimum percentile eluted ligand likelihood (% rank) was extracted for each peptide. The distributions of % rank values of epitopes and non-epitopes were then compared with the [*medianBootstrap*](https://doi.org/10.1111/nph.17159) method, using 10,000 trials.

![Immunoinformatics benchmarking](Figures/raw/newpvalue_singlesstatusprocessediedbepitopes_minrank_plot_withpvalue.png){width=500 height=300px}

HLA class I benchmarking was not performed due to the low number of negative peptide-HLA class I pairs obtained (n=29).

Code for this section is contained in the scripts [**16-Immunoinformatics-benchmarking_getknownepitopes.sh**](Scripts/16-Immunoinformatics-benchmarking_getknownepitopes.sh), [**17-Immunoinformatics-benchmarking_getknownepitopes_mhc1.sh**](Scripts/17-Immunoinformatics-benchmarking_getknownepitopes_mhc1.sh), [**18-Immunoinformatics-benchmarking_getrankvalues.sh**](Scripts/18-Immunoinformatics-benchmarking_getrankvalues.sh) and [**19-Immunoinformatics-benchmarking_statisticsandplot.R**](Scripts/19-Immunoinformatics-benchmarking_statisticsandplot.R).
