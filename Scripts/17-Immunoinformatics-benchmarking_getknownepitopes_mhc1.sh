cd "$( dirname -- "$0"; )" ;
mkdir -p ../Immunoinformatics/iedbepitopes/MHC1 ;
cd ../Immunoinformatics/iedbepitopes/MHC1 ;


echo "Create MHC1 peptide epitope list" ;
#---------------------------------------------------------------------------------------
## vargas2021 will be discarded, as it does not have MHC1or2 info...
less ../../../NGS_helper_files/iedbepitopes_all_menardo2021.tsv \
| awk -F'\t' '$4 ~ "MHC1" {print $7}' OFS='\t' \
| less \
| sort -u \
| less \
>iedbepitopes_mhc1only_menardo2021.mhc1 ;
wc -l iedbepitopes_mhc1only_menardo2021.mhc1 ;
## 539 unique MHC1-binding peptides!
head -3 iedbepitopes_mhc1only_menardo2021.mhc1 ;
less iedbepitopes_mhc1only_menardo2021.mhc1 \
| sort \
| uniq -c \
| awk '$1>1' \
| wc -l ;
## no duplicated peptides present, good!


echo "Get peptide list for MHC1 predictions" ;
#---------------------------------------------------------------------------------------  
less iedbepitopes_mhc1only_menardo2021.mhc1 \
| awk 'length($1)>=9 && length($1)<=14' \
| less \
>iedbepitopes_mhc1only_menardo2021_9to14aa.mhc1 ;
head -3 iedbepitopes_mhc1only_menardo2021_9to14aa.mhc1 ;
wc -l iedbepitopes_mhc1only_menardo2021_9to14aa.mhc1 ;
## 367 peptide epitopes!
### 539-367 = 172 peptides excluded (only 6 peptides with 8aa)!


echo "Compare T cell epitopes from Menardo2021 to the full set in IEDB" ;
#---------------------------------------------------------------------------------------  
## epitopes extracted from iedb.org on 8/09/2022 -> Linear peptide ; Organism=Mycobacterium tuberculosis complex (same peptides as individual strains selection) ; Human host ; T cell and MHC ligand assays (positive and then negative ; MHC ligand only after exporting T cell-only results -> main epitope list is not the same when the two are combined) ; class I MHC restriction
### 4 files obtained, 1 for T cell assays + 1 for MHC ligand assays (repeat for negative) and saved as 'tcellepitopes_classI_*.csv'!

# Get peptide,HLA,year,PubMedID and pos/negative tag file:
#--------------------------------------
head -3 tcellepitopes_classI_positive_assays_tcell.csv | cat -n ;
head -3 tcellepitopes_classI_negative_assays_tcell.csv | cat -n ;
## 2 header rows!

cat <(awk 'NR>2 {print $0,"positive"}' OFS='\t' tcellepitopes_classI_positive_assays_tcell.csv | less) <(awk 'NR>2 {print $0,"negative"}' OFS='\t' tcellepitopes_classI_negative_assays_tcell.csv | less) \
| less \
| sed -e 's/",/\t/g' -e 's/"//g' \
| less \
| awk -F'\t' '{print $12,$102,$7,$4,$NF}' OFS='\t' \
| less \
>tcellepitopes_classI_tcellassays_joined.tsv ;
head -3 tcellepitopes_classI_tcellassays_joined.tsv ;
wc -l tcellepitopes_classI_tcellassays_joined.tsv ;
## 1258 rows!
### HLA restriction data present!!
### through references tab on IEDB search, it was possible to verify that there are 2 studies published after 03-08-2020 that were not possibly included into Menardo2021 list!
less tcellepitopes_classI_tcellassays_joined.tsv \
| grep 'positive' \
| less \
| awk -F'\t' '$3>2019' OFS='\t' \
| less \
| cut -f1 \
| sort -u \
| less \
| grep -w -F -f /dev/stdin iedbepitopes_mhc1only_menardo2021_9to14aa.mhc1 \
| less \
| wc -l ;
## 5 of the 20 possibly novel peptides were already considered in Menardo2021!
### 20-5=15 peptides to evaluate...
less tcellepitopes_classI_tcellassays_joined.tsv \
| cut -f1 \
| sort -u \
| less \
| grep -v -w -F -f /dev/stdin iedbepitopes_mhc1only_menardo2021_9to14aa.mhc1 \
| less \
| wc -l ;
## there are no novel epitopes in menardo2021 list!, but there are negative epitopes in menardo2021 list!


echo "Investigate whether the same epitope is simultaneously positive and negative for the same HLA" ;
#---------------------------------------------------------------------------------------  
# Get peptide,HLA,mhcligandortcellassay tag and pos/negative tag file:
#--------------------------------------
head -3 tcellepitopes_classI_positive_assays_mhcligand.csv | cat -n ;
head -3 tcellepitopes_classI_negative_assays_mhcligand.csv | cat -n ;
## 2 header rows!

cat <(awk 'NR>2 {print $0,"mhcligand","positive"}' OFS='\t' tcellepitopes_classI_positive_assays_mhcligand.csv | less) <(awk 'NR>2 {print $0,"mhcligand","negative"}' OFS='\t' tcellepitopes_classI_negative_assays_mhcligand.csv | less) \
| less \
| sed -e 's/",/\t/g' -e 's/"//g' \
| less \
| awk -F'\t' '{print $12,$81,$(NF-1),$NF}' OFS='\t' \
| less \
| cat /dev/stdin <(awk -F'\t' '{print $1,$2,"tcell",$NF}' OFS='\t' tcellepitopes_classI_tcellassays_joined.tsv | less) \
| less \
| grep -v '\+' \
| less \
>tcellepitopes_classI_mhcligandandtcell_joined.tsv ;
head -3 tcellepitopes_classI_mhcligandandtcell_joined.tsv ;
wc -l tcellepitopes_classI_mhcligandandtcell_joined.tsv ;
grep -w 'mhcligand' tcellepitopes_classI_mhcligandandtcell_joined.tsv -c ;
## 10989 rows (9731 for MHC ligand assays)

less tcellepitopes_classI_mhcligandandtcell_joined.tsv \
| cut -f2 \
| sort -u \
| cat -n ;
## 93 HLA class I allele forms (8 abbreviated forms and 'H2 class I" ; no HLA-E or HLA-G!)
### HLA-A2: it's a general term (https://en.wikipedia.org/wiki/HLA-A*02)
### HLA-A24: it's a general term (https://en.wikipedia.org/wiki/HLA-A24)
### HLA-B14: it's a general term (https://en.wikipedia.org/wiki/HLA-B14)
### HLA-B35: it's a general term (https://en.wikipedia.org/wiki/HLA-B35)
### HLA-B44: it's a general term (https://en.wikipedia.org/wiki/HLA-B44)
### HLA-B52: it's a general term (https://en.wikipedia.org/wiki/HLA-B52), but 84% HLA-B*52:01
### HLA-B53: it's a general term (https://en.wikipedia.org/wiki/HLA-B53)
### HLA-B7: it's a general term (https://en.wikipedia.org/wiki/HLA-B7)
### each general term is unique and cannot be grouped together, as in MHC2...

## strategy: group by peptide, filter the ones with positive+negative, watch out for the corresponding abbreviated forms grouping, including with 4-digit resolution HLAs!
less tcellepitopes_classI_mhcligandandtcell_joined.tsv \
| grep 'HLA-' \
| less \
| datamash -s -g 1 unique 2,3,4 countunique 2,3,4 \
| less \
| awk -F'\t' '$(NF-2)>1 && $NF>1' OFS='\t' \
| less \
| grep -e 'HLA-A\*02:.*HLA-A2' -e 'HLA-A\*24:.*HLA-A24' -e 'HLA-B\*14:.*HLA-B14' -e 'HLA-B\*35:.*HLA-B35' -e 'HLA-B\*44:.*HLA-B44' -e 'HLA-B\*52:.*HLA-B52' -e 'HLA-B\*53:.*HLA-B53' -e 'HLA-B\*07:.*HLA-B7' \
| cat -n \
| grep -e 'HLA-A2' -e 'HLA-B[1-7]' ;
## 21 peptides with 'positive' and 'negative' results and with restriction to general HLA-[A-B] terms and its associated specific HLAs... -> 21/1641 ≃ 1.3% of the peptides -> a very small fraction...
## conclusion: discordant positive/negative status will be checked with grouping by individual HLA!


# peptides to exclude due to positive+negative status for the same HLA:
#------------------------------------------------------
less tcellepitopes_classI_mhcligandandtcell_joined.tsv \
| grep -v 'H2 class I' \
| less \
| datamash -s -g 1,2 unique 3,4 countunique 3,4 \
| less \
| awk -F'\t' '$NF>1' OFS='\t' \
| less \
>toexclude_HLAclassIcount_tcellandmhcligand_joined.tsv ;
head -3 toexclude_HLAclassIcount_tcellandmhcligand_joined.tsv ;
wc -l toexclude_HLAclassIcount_tcellandmhcligand_joined.tsv ;
## 231 peptides found!

## let's check HLA distribution of excluded peptides:
cut -f2 toexclude_HLAclassIcount_tcellandmhcligand_joined.tsv \
| sort \
| uniq -c \
| cat -n ;
## 18 HLA alleles (including 11 'HLA class I') ; 141 / 231 is from HLA-A*02:01!!
### before excluding peptides from the whole process, we must check if they are found with non-conflicting individual HLA data:
less toexclude_HLAclassIcount_tcellandmhcligand_joined.tsv \
| datamash -s -g 1 unique 2 countunique 2 \
| less \
| grep -e 'HLA class I' -e 'HLA-A[1-9]' -e 'HLA-B[1-9]' \
| awk -F'\t' '$NF>1' OFS='\t' \
| cat -n ;
## 2 peptides, 1 to check for discording positive/negative data
grep -w 'ELNNALQNL' tcellepitopes_classI_mhcligandandtcell_joined.tsv ;
## peptide will not be excluded for 'HLA-A*24:02' (all 'positive'), only for 'HLA-A24' if needed...

## let's see how many peptides will be excluded from the whole process or just for specific HLA alleles:
### the logic is to find peptides in excluded file that do not have additional HLA restriction data...
less toexclude_HLAclassIcount_tcellandmhcligand_joined.tsv \
| awk -F'\t' 'NR==FNR {pepHLA[$1 FS $2] ; next} !($1 FS $2 in pepHLA)' OFS='\t' /dev/stdin <(grep -v 'H2 class I' tcellepitopes_classI_mhcligandandtcell_joined.tsv | less) \
| less \
| awk -F'\t' 'NR==FNR {peptide[$1] ; next} !($1 in peptide)' OFS='\t' /dev/stdin toexclude_HLAclassIcount_tcellandmhcligand_joined.tsv \
| less \
>toexclude_allHLAsclassI_tcellandmhcligand_joined.tsv ;
head -3 toexclude_allHLAsclassI_tcellandmhcligand_joined.tsv ;
wc -l toexclude_allHLAsclassI_tcellandmhcligand_joined.tsv ;
## 142/231 peptides can be excluded from the whole process!

awk -F'\t' 'NR==FNR {peptide[$1] ; next} !($1 in peptide)' OFS='\t' toexclude_allHLAsclassI_tcellandmhcligand_joined.tsv toexclude_HLAclassIcount_tcellandmhcligand_joined.tsv \
| less \
| grep -v ':' \
| cat -n ;
## only 12 discordant peptides with general terms, such as "HLA-A2", "HLA class I" and "HLA-A24". Similarly to HLA class II, these won't be considered 'specificHLAs'...
awk -F'\t' 'NR==FNR {peptide[$1] ; next} !($1 in peptide)' OFS='\t' toexclude_allHLAsclassI_tcellandmhcligand_joined.tsv toexclude_HLAclassIcount_tcellandmhcligand_joined.tsv \
| less \
| grep ':' \
| less \
>toexclude_specificHLAsclassI_tcellandmhcligand_joined.tsv ;
head -3 toexclude_specificHLAsclassI_tcellandmhcligand_joined.tsv ;
wc -l toexclude_specificHLAsclassI_tcellandmhcligand_joined.tsv ;
## 77 peptide/HLA pairs not to be considered after immunoinformatics predictions!!
### similarly to HLA class II, decided to consider 'toexclude_HLAclassIcount_tcellandmhcligand_joined.tsv' as the peptide list to exclude -> more conservative approach...


echo "Get final T cell epitope list to test" ;
#---------------------------------------------------------------------------------------  
less tcellepitopes_classI_tcellassays_joined.tsv \
| grep -v 'H2 class I' \
| less \
| awk -F'\t' 'NR==FNR {pepHLA[$1 FS $2] ; next} !($1 FS $2 in pepHLA)' OFS='\t' toexclude_HLAclassIcount_tcellandmhcligand_joined.tsv /dev/stdin \
| less \
>concordantstatus_classI_tcellassays_joined.tsv ;
head -3 concordantstatus_classI_tcellassays_joined.tsv ;
wc -l concordantstatus_classI_tcellassays_joined.tsv ;
## 932 entries!

less concordantstatus_classI_tcellassays_joined.tsv \
| datamash -s -g 1,2 unique 5 countunique 5 \
| less \
| awk -F'\t' '$NF>1' OFS='\t' ;
## no peptide/HLA pair with discordant status, confirmed!
less concordantstatus_classI_tcellassays_joined.tsv \
| datamash -s -g 1 unique 2 countunique 2 unique 5 countunique 5 \
| less \
| awk -F'\t' '$NF>1' OFS='\t' \
| cat -n ;
## 18 peptides with different status (due to different HLA restrictions...)
### some peptides might need to be excluded, due to discordant status on different (but equivalent) general group terms!...
grep -w -e 'IPAEFLENF' -e 'MPVGGQSSF' -e 'WPTLIGLAM' concordantstatus_classI_tcellassays_joined.tsv \
| less \
| grep '35' \
| cat -n ;
## all 'positive' in 'HLA-B35' and 'HLA-B*35:01', so nothing to exclude here!
grep -w 'MPVGGQSSF' concordantstatus_classI_tcellassays_joined.tsv \
| less \
| grep -e '07' -e 'B7' \
| cat -n ;
## discordant 'HLA-B*07:02' and 'HLA-B7', so the solution might be to consider only the HLAs of the least general term after immunoinformatics predictions!

less concordantstatus_classI_tcellassays_joined.tsv \
| cut -f2 \
| sort \
| uniq -c ;
## greatest majority of entries (612/932 ≃ 66%) is from 'HLA class I' (331) + 'HLA-A*02:01' (281)...

less concordantstatus_classI_tcellassays_joined.tsv \
| grep ':.*negative' \
| cat -n ;
## only 29 negative peptide-HLA pairs...
### number of negative peptides is not suficient for a robust ROC with 4-digit HLAs...

awk -F'\t' 'length($1)<9 || length($1)>14 {print $1,length($1)}' concordantstatus_classI_tcellassays_joined.tsv \
| less \
| sort -u \
| sort -nk2,2 \
| less \
| wc -l ;
## 195/932 peptides with no possibility to test in immunoinformatics programs...
### only 7 peptides with 8aa length...

grep 'positive' concordantstatus_classI_tcellassays_joined.tsv \
| awk -F'\t' 'length($1)>8 && length($1)<=14 {print $1}' \
| sort -u \
| wc -l ;
## 251 unique positive peptides!
grep 'negative' concordantstatus_classI_tcellassays_joined.tsv \
| awk -F'\t' 'length($1)>8 && length($1)<=14 {print $1}' \
| sort -u \
| wc -l ;
## 56 unique negative peptides! (it is not balanced but at least it might be sufficient for a ROC or to get tendencies with nonparametric statistics...)
awk -F'\t' 'length($1)>8  && length($1)<=14 {print $1}' concordantstatus_classI_tcellassays_joined.tsv \
| less \
| sort -u \
| less \
>peps_concordantstatus_classI_tcellassays_joined.mhc1 ;
head -3 peps_concordantstatus_classI_tcellassays_joined.mhc1 ;
wc -l peps_concordantstatus_classI_tcellassays_joined.mhc1 ;
## 289 peptides to possibly test in immunoinformatics!!
### values is lower than 251+56=307 because the same peptide can be simultaneously positive for some HLAs and negative for others...


echo "Compress and remove large files" ;
#---------------------------------------------------------------------------------------
du -shc * | sort -rh ;
## 12MB total files!
ls tcellepitopes_classI_*tive_*.csv* \
| less \
>compressedandremovedfiles_mhc1iedbepitopes.txt ;
less compressedandremovedfiles_mhc1iedbepitopes.txt \
| while read file ;
  do echo filetocompress="$file" ;
    tar -czvf "$file".tar.gz "$file" ;
    rm "$file" ;
done ;
du -shc * | sort -rh ;
## <1MB total files after compression!
